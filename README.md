# holoCode

framaPad: <https://annuel2.framapad.org/p/holoCode>

### What is holoCode ?

1. first and formost this it the krystal code, the fundamental mathematics on which everything is based !

3. the "[krysthalSphere OS][1]" open source code itself (i.e. the [GIT repositories][2])
4. the set of rules and conventions used to operate and maintain coherence w/i each holosphere




## Mathematics :


* Krystal Code Integer Series : [0,1,1,2,4,8,16,...][3] ([A166444])
* Fibonacii Code Integer Series : [0,1,1,2,3,5,8,13,21,...][A000045]


however the 5-ary [fibonacci][4] sequence (called pentanacci) has the same start see also [urn:OEIS:A000127](https://oeis.org/A000127)

Fibonacci and Krystal ... what is the differences ?
    
  see also
  * https://annuel2.framapad.org/p/holoCode and 
  * https://annuel2.framapad.org/p/fiboCode

![image](https://gateway.ipfs.io/ipfs/QmZGEoZy6ZhmzAASmzU1GmzdhK2UXxHMc31qeTkQnYSnVq/5-bonacci.png)


see also
 - Euler's formula & [circle division](https://www.youtube.com/watch?v=K8P8uFahAgc) (not Krysthal)
 - Guy 1990: [Strong Law of Small Numbers](https://mathworld.wolfram.com/StrongLawofSmallNumbers.html) ([A005181] not Fibonacci)


[A000045]: https://oeis.org/A000045
[A000127]: https://oeis.org/A000127
[A005181]: https://oeis.org/A005181
[A166444]: https://oeis.org/A166444
[A00322]: https://oeis.org/wiki/N-bonacci_numbers

[1]: https://qwant.com/?q=%22krysthalSphere+OS%22+%26g
[2]: https://gitlab.com/kinLabs
[3]: https://duckduckgo.com/?q="0,1,1,2,4,8,16"+!g
[4]: https://oeis.org/wiki/N-bonacci_numbers
