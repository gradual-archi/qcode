#!/usr/bin/perl


# Fibonacci : Fn = Fn-1 + Fn-2 

my $n = 10;
my $n2 = $n + 2;
# Fibonnacci Binet formula ... (rounding error above n = 70)
my $sq5 = sqrt(5);
my $binet = ( (1 + $sq5) ** $n2 - (1 - $sq5) ** $n2 ) / ( 2 ** $n2 * $sq5);
my $scale = 100 / $binet;

# phi / x^2 = x + 1;
my $phi = (1 + $sq5) / 2;
#E Standard Series: r = 10^i/96

my $fim2 = 0;
my $fim1 = 1;
# sum = fn+2 - 1
my $sum = 1;
for my $i (2 .. $n2) {
 my $fi = $fim1 + $fim2;
 my $d = int($fi * $scale +0.4999);
 $sum += $d;
 printf "%u, %2s/%-2s, %2s, %2s\n",$i,$fi,$fi/$phi,$d,$sum;
 $fim2 = $fim1;
 $fim1 =$fi;
}
